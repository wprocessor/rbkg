<?php

namespace CurrencyCalculatorBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller as BaseController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use CurrencyCalculatorBundle\Entity\CurrencyCalculatorInput;

/*
 * Base CurrencyCalculator App controller which defined action
 * to perform currency conversation on users requests
 */
class CurrencyCalculatorController extends BaseController
{
  /*
   * Main action
   */
  public function indexAction(Request $request) {
    // Check that table is filled.
    $this->get('currency_calculator.currency_updater')->checkSystemConsistance();

    $translator = $this->get('translator');
    $currencyCalculatorInput = new CurrencyCalculatorInput();

    $currencyList = [
      'class' => 'CurrencyCalculatorBundle:CurrencyType',
      'choice_label' => 'currencyName',
      'choice_value' => 'currencyName',
      'label' => $translator->trans('From Currency'),
    ];

    $form = $this->createFormBuilder($currencyCalculatorInput)
      ->add('currencyTypeFrom', EntityType::class, $currencyList)
      ->add('currencyTypeFromAmount', NumberType::class, ['label' => $translator->trans('Amount')]);

    $currencyList['label'] = $translator->trans('To Currency');
    $form = $form
      ->add('currencyTypeTo', EntityType::class, $currencyList)
      ->add('save', SubmitType::class, array('label' => $translator->trans('Calculate currency')));

    $formResult = $form->getForm();
    $formResult->handleRequest($request);

    if ($formResult->isSubmitted() && $formResult->isValid()) {
      
      $calculator = $this->get('currency_calculator.currency_super_calculator');
      $currencyTypeToAmount = $calculator->calculate(
        $currencyCalculatorInput->getCurrencyTypeFrom(),
        $currencyCalculatorInput->getCurrencyTypeFromAmount(),
        $currencyCalculatorInput->getCurrencyTypeTo()
      );

      $formResult = $form
        ->add('currencyTypeToAmount', TextType::class, ['data' => $currencyTypeToAmount, 'disabled' => true])
        ->getForm();
    }

    return $this->render(
      'CurrencyCalculatorBundle:Home:currency-calculator-home-page.html.twig', [
        'form' => $formResult->createView(),
      ]
    );
  }
}