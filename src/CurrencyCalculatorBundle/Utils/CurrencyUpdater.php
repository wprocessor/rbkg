<?php

namespace CurrencyCalculatorBundle\Utils;

use Symfony\Component\DependencyInjection\ContainerInterface;
use CurrencyCalculatorBundle\Exceptions\CurrencyCalculatorInvalidUrlOrConnectionException;
use CurrencyCalculatorBundle\Entity\CurrencyType;
use CurrencyCalculatorBundle\Entity\CurrencyUpdates;

/*
 * Class to check actuality of currency table
 */
class CurrencyUpdater {
  /*
   * container
   */
  private $container;
  /*
   * Constructed class & add container to manage site data
   */
  public function __construct(ContainerInterface $container) {
    $this->container = $container;
  }
  /*
   * Check currency info state in DB
   * and reload if data is up to date
   */
  public function checkSystemConsistance() {
    $updates = $this->container->get('doctrine')->getRepository('CurrencyCalculatorBundle:CurrencyUpdates');
    $updateObject = $updates->findOneBy(['name' => 'currency']);

    $currencyApi = $this->container->get('currency_calculator.currency_manager');
    $currencyObject = $currencyApi->getCurrencyObject();
    $xmlUpdate = \DateTime::createFromFormat('Ymd', $currencyObject->date);
    $xmlUpdate->setTime(0, 0);

    if (empty($updateObject) || $xmlUpdate != $updateObject->getUpdateAt()) {
      $this->updateCurrencies($currencyObject);
      $manager = $this->container->get('doctrine')->getManager();
      $updateItem = empty($updateObject) ? new CurrencyUpdates() : $updateObject;
      $updateDate = \DateTime::createFromFormat('Ymd', $currencyObject->date);
      $updateItem->setName('currency');
      $updateItem->setUpdateAt($updateDate);
      $manager->persist($updateItem);
      $manager->flush();
    }
  }
  /*
   * Update Db currency table from external Api data
   */
  protected function updateCurrencies($currencyObject) {
    $currencies = $this->container->get('doctrine')->getRepository('CurrencyCalculatorBundle:CurrencyType');
    $manager = $this->container->get('doctrine')->getManager();

    foreach ($currencyObject->list as $currencyName => $currencyValue) {
      $currencyItem = $currencies->findOneBy([
        'currencyName' => $currencyName
      ]);

      if (!$currencyItem) {
        $currencyItem = new CurrencyType();
        $currencyItem->setCurrencyName($currencyName);
        $currencyItem->setCurrencyValue($currencyValue * 100000);
      }

      $manager->persist($currencyItem);
      $manager->flush();
    }
  }
}
