<?php

namespace CurrencyCalculatorBundle\Utils;

use CurrencyCalculatorBundle\Exceptions\CurrencyCalculatorInvalidUrlOrConnectionException;
use CurrencyCalculatorBundle\Exceptions\CurrencyCalculatorInvalidSchemaOrConnection;

/*
 * Currency service to work with external API
 * to get currency exchange data
 */
class CurrencyManager
{
  /*
   * External xml url to currency API
   */
  private $xmlpath;
  /*
   * External xsd url to validate xml currency API
   */
  private $xsdpath;
  /*
   * External xsd url to validate xml currency API
   */
  private $xmldoc_cache;
  
  /*
   * Constructor to initialize properties
   */
  public function __construct($xmlpath, $xsdpath) {
    $this->xmlpath = $xmlpath;
    $this->xsdpath = $xsdpath;
  }
  /*
   * Just to save Xml doc to avoid everytime loading
   */
  private function getXmlDoc() {
    if (empty($this->xmldoc_cache)) {
      $this->xmldoc_cache = new \DOMDocument();

      if (empty($xmlContent = @file_get_contents($this->xmlpath))) {
        throw new CurrencyCalculatorInvalidUrlOrConnectionException();
      }
      $this->xmldoc_cache->loadXml($xmlContent);
      
    }

    return $this->xmldoc_cache;
  }
  /*
   * Check xmldoc by xsd schema
   */
  public function checkAPI() {
    $xmldoc = $this->getXmlDoc();
    if ($validationResult = @$xmldoc->schemaValidate($this->xsdpath)) {
      return $validationResult;
    }
    
    throw new CurrencyCalculatorInvalidSchemaOrConnection();
  }
  /*
   * Get currency object which contains
   * actual date, and list of currencies
   */
  public function getCurrencyObject() {
    $this->checkAPI();
    $xmldoc = $this->getXmlDoc();

    $date = $xmldoc->getElementsByTagName('Date')->item(0)->nodeValue;
    $currencies = $xmldoc->getElementsByTagName('Currency');

    $currency = new \stdClass();
    $currency->date = $date;
    $currency->list = ['EUR' => 1];

    foreach ($currencies as $nextCurrency) {
      $currencyName = $nextCurrency->getElementsByTagName('ID')->item(0)->nodeValue;
      $currencyValue = $nextCurrency->getElementsByTagName('Rate')->item(0)->nodeValue;
      
      $currency->list[$currencyName] = $currencyValue;
    }
    
    return $currency;
  }
}