<?php

namespace CurrencyCalculatorBundle\Utils;

use Symfony\Component\DependencyInjection\ContainerInterface;

/*
 * Super mega calculator
 */
class CurrencySuperCalculator {
  /*
   * container
   */
  private $container;
  /*
   * Constructed class & add container to manage site data
   */
  public function __construct(ContainerInterface $container) {
    $this->container = $container;
  }
  /*
   * Secret calculation algorithm
   */
  public function calculate($currencyFrom, $currencyFromAmount, $currencyTo) {
    $currencies = $this->container->get('doctrine')->getRepository('CurrencyCalculatorBundle:CurrencyType');

    $currencyFromItem = $currencies->findOneBy([
      'currencyName' => $currencyFrom->getCurrencyName(),
    ]);
    $currencyToItem = $currencies->findOneBy([
      'currencyName' => $currencyTo->getCurrencyName(),
    ]);

    return ((int)(($currencyToItem->getCurrencyValue() / $currencyFromItem->getCurrencyValue()) * $currencyFromAmount * 100)) / 100;
  }
}