<?php

namespace CurrencyCalculatorBundle\Entity;

/*
 * This is a class helper which is
 * container for user input data,
 * validate fields
 */
class CurrencyCalculatorInput {
  private $currencyTypeFrom;
  private $currencyTypeFromAmount;
  private $currencyTypeTo;
  private $currencyTypeToAmount;

  public function getCurrencyTypeFrom() {
    return $this->currencyTypeFrom;
  }
  public function setCurrencyTypeFrom($value) {
    $this->currencyTypeFrom = $value;
  }

  public function getCurrencyTypeFromAmount() {
    return $this->currencyTypeFromAmount;
  }
  public function setCurrencyTypeFromAmount($value) {
    $this->currencyTypeFromAmount = $value;
  }
  public function getCurrencyTypeTo() {
    return $this->currencyTypeTo;
  }
  public function setCurrencyTypeTo($value) {
    $this->currencyTypeTo = $value;
  }

  public function getCurrencyTypeToAmount() {
    return $this->currencyTypeToAmount;
  }
  public function setCurrencyTypeToAmount($value) {
    $this->currencyTypeToAmount = $value;
  }
}