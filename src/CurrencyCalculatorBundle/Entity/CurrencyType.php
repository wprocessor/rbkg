<?php

/*
 * This file contains model CurrencyType
 * which contains info about currencies & their values
 */

namespace CurrencyCalculatorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="currency")
 */
class CurrencyType
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @ORM\Column(type="string", length=3)
     */
    private $currencyName;
    /**
     * @ORM\Column(type="integer")
     */
    private $currencyValue;
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set currencyName
     *
     * @param string $currencyName
     *
     * @return CurrencyType
     */
    public function setCurrencyName($currencyName)
    {
        $this->currencyName = $currencyName;

        return $this;
    }

    /**
     * Get currencyName
     *
     * @return string
     */
    public function getCurrencyName()
    {
        return $this->currencyName;
    }

    /**
     * Set currencyValue
     *
     * @param integer $currencyValue
     *
     * @return CurrencyType
     */
    public function setCurrencyValue($currencyValue)
    {
        $this->currencyValue = $currencyValue;

        return $this;
    }

    /**
     * Get currencyValue
     *
     * @return integer
     */
    public function getCurrencyValue()
    {
        return $this->currencyValue;
    }
}
