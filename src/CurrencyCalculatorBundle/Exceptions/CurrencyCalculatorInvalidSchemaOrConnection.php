<?php

namespace CurrencyCalculatorBundle\Exceptions;

/*
 * Except on schema api url or connection problems
 */
class CurrencyCalculatorInvalidSchemaOrConnection extends \Exception implements CurrencyCalculatorExceptionInterface {
  /*
   * {@inheritdoc}
   */
  public function __construct ($message = '', $code = 0, $previous = null) {
    parent::__construct('CurrencyCalculatorBundle: Bad url to schema API xsd or connection problem', $code, $previous);
  }
}
