<?php

namespace CurrencyCalculatorBundle\Exceptions;

/*
 * Except on api url or connection problems
 */
class CurrencyCalculatorInvalidUrlOrConnectionException extends \Exception implements CurrencyCalculatorExceptionInterface {
  /*
   * {@inheritdoc}
   */
  public function __construct ($message = '', $code = 0, $previous = null) {
    parent::__construct('CurrencyCalculatorBundle: Bad url to API xml or connection problem', $code, $previous);
  }
}
