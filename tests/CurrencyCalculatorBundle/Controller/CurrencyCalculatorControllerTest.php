<?php

namespace Tests\CurrencyCalculatorBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/*
 * class to test currency calculator page
 */
class CurrencyCalculatorControllerTest extends WebTestCase
{
  /*
   * test form with GET method
   */
  public function testShowForm() {
    $client = static::createClient();
    $crawler = $client->request('GET', '/currency-calculator/');

    $this->assertCount(1, $crawler->filter('h1'));
    $this->assertCount(1, $crawler->filter('form'));
    $this->assertCount(1, $crawler->filter('select#form_currencyTypeFrom'));
    $this->assertCount(1, $crawler->filter('input#form_currencyTypeFromAmount'));
    $this->assertCount(1, $crawler->filter('select#form_currencyTypeTo'));
    $this->assertCount(1, $crawler->filter('button#form_save'));
  }
  /*
   * test form with POST method
   */
  public function testFormOnSuccess() {
    $client = static::createClient();
    $crawler = $client->request('GET', '/currency-calculator/');

    $form = $crawler->filter('button#form_save')->form();
 
    $form['form[currencyTypeFrom]'] = 'EUR';
    $form['form[currencyTypeFromAmount]'] = 100;
    $form['form[currencyTypeTo]'] = 'EUR';

    $client->submit($form);
    
    $this->assertEquals(200, $client->getResponse()->getStatusCode());
    $this->assertContains('form[currencyTypeToAmount]" disabled="disabled" required="required" value="100', $client->getResponse()->getContent());
  }
}